﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace death_roll
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            string pn1;
            int pr1;
            int ran1;
            string cn1;
            int cr1;
            int ran2;
            string pn2;
            int pr2;
            int ran3;
            string cn2;
            int cr2;
            int ran4;
            string pn3;
            int pr3;
            int ran5;
            string cn3;
            int cr3;
            int ran6;
            string pn4;
            int pr4;
            int ran7;
            string cn4;
            int cr4;
            int ran8;
            string pn5;
            int pr5;
            int ran9;
            string cn5;
            int cr5;
            int ran10;



            MyGlobals.Rounds = MyGlobals.Rounds + 1;
            label6.Text = MyGlobals.Rounds.ToString();

            if (MyGlobals.Rounds == 1)
            {
                pn1 = textBox1.Text;
                pr1 = Int32.Parse(pn1);
                ran1 = rand.Next(pr1);
                label3.Text = ran1.ToString();
                if (label3.Text == "0")
                {
                    MessageBox.Show("You Lost!");
                    System.Windows.Forms.Application.ExitThread();
                }
                MessageBox.Show("Computer Rolling...");
                Thread.Sleep(1000);
                cn1 = label3.Text;
                cr1 = Int32.Parse(cn1);
                ran2 = rand.Next(cr1);
                label4.Text = ran2.ToString();
                if (label4.Text == "0")
                {
                    MessageBox.Show("You Win!");
                    System.Windows.Forms.Application.ExitThread();
                }
            }
            if (MyGlobals.Rounds == 2)
            {
                pn2 = label4.Text;
                pr2 = Int32.Parse(pn2);
                ran3 = rand.Next(pr2);
                label3.Text = ran3.ToString();
                if (label3.Text == "0")
                {
                    MessageBox.Show("You Lost!");
                    System.Windows.Forms.Application.ExitThread();
                }
                MessageBox.Show("Computer Rolling...");
                Thread.Sleep(1000);
                cn2 = label3.Text;
                cr2 = Int32.Parse(cn2);
                ran4 = rand.Next(cr2);
                label4.Text = ran4.ToString();
                if (label4.Text == "0")
                {
                    MessageBox.Show("You Win!");
                    System.Windows.Forms.Application.ExitThread();
                }
            }
            if (MyGlobals.Rounds == 3)
            {
                pn3 = label4.Text;
                pr3 = Int32.Parse(pn3);
                ran5 = rand.Next(pr3);
                label3.Text = ran5.ToString();
                if (label3.Text == "0")
                {
                    MessageBox.Show("You Lost!");
                    System.Windows.Forms.Application.ExitThread();
                }
                MessageBox.Show("Computer Rolling...");
                Thread.Sleep(1000);
                cn3 = label3.Text;
                cr3 = Int32.Parse(cn3);
                ran6 = rand.Next(cr3);
                label4.Text = ran6.ToString();
                if (label4.Text == "0")
                {
                    MessageBox.Show("You Win!");
                    System.Windows.Forms.Application.ExitThread();
                }
            }
            if (MyGlobals.Rounds == 4)
            {
                pn4 = label4.Text;
                pr4 = Int32.Parse(pn4);
                ran7 = rand.Next(pr4);
                label3.Text = ran7.ToString();
                if (label3.Text == "0")
                {
                    MessageBox.Show("You Lost!");
                    System.Windows.Forms.Application.ExitThread();
                }
                MessageBox.Show("Computer Rolling...");
                Thread.Sleep(1000);
                cn4 = label3.Text;
                cr4 = Int32.Parse(cn4);
                ran8 = rand.Next(cr4);
                label4.Text = ran8.ToString();
                if (label4.Text == "0")
                {
                    MessageBox.Show("You Win!");
                    System.Windows.Forms.Application.ExitThread();
                }
            }
            if (MyGlobals.Rounds == 5)
            {
                pn5 = label4.Text;
                pr5 = Int32.Parse(pn5);
                ran9 = rand.Next(pr5);
                label3.Text = ran9.ToString();
                if (label3.Text == "0")
                {
                    MessageBox.Show("You Lost!");
                    System.Windows.Forms.Application.ExitThread();
                }
                MessageBox.Show("Computer Rolling...");
                Thread.Sleep(1000);
                cn5 = label3.Text;
                cr5 = Int32.Parse(cn5);
                ran10 = rand.Next(cr5);
                label4.Text = ran10.ToString();
                if (label4.Text == "0")
                {
                    MessageBox.Show("You Win!");
                    System.Windows.Forms.Application.ExitThread();
                }
            }
        }
        public static class MyGlobals
        {
            public static int Rounds;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
  
        }
    }
}
